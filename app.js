

const path = require('path');
const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');
const fs = require('fs');
// const https = require('https');
const graphqlHttp = require('express-graphql');

// Middlewares
const auth = require('./middlewares/auth');
// Services
const fsUtils = require('./services/fs-utils');

// Loading graphql files
const graphqlSchema = require('./graphql/schema');
const graphqlResolver = require('./graphql/resolvers');

// Loading env variables
const dotEnv = require('dotenv').config();
const port = process.env.PORT || '3000';
const MONGODB_PREFIX = process.env.MONGODB_PREFIX;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_ROOT_PASSWORD = process.env.DB_ROOT_PASSWORD;
const DB_HOST = process.env.DB_HOST;
const DB_NAME = process.env.DB_NAME;
const MONGODB_URL_RULE = process.env.MONGODB_URL_RULE;

const MONGODB_URL = `${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/${DB_NAME}?${MONGODB_URL_RULE}`


const app = express();
// ssl / tls
// const privateKey = fs.readFileSync('server.key');
// const certificate = fs.readFileSync('server.cert');

// security for http header
app.use(helmet());
// compression handler
app.use(compression());
// log handler
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs', 'access.log'), {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));

// body parsing middleware
app.use(bodyParser.json()); // application/json
// CORS Error Handling
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  // graphql setting
  if(req.method === 'OPTIONS'){
    return res.sendStatus(200);
  }

  next();
});
// image middleware parser
// filtering images
const fileFilter = (req, file, cb) => {
  if(file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || 'image/jpg'){
    // accept download
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, `${new Date().toISOString()}-${file.originalname}`);
  }
});
app.use(multer({storage: fileStorage}).single('image'));

// images static serving
app.use('/images', express.static(path.join(__dirname, 'images')));

// Authorization check middleware
app.use(auth);

// REST API Endpoint for image Upload
app.put('/image-upload', (req, res, next) => {

  // Authorization checking
  if(!req.isAuth){
    throw new Error('Not Authenticated');
  }
  //if no file (multer parsing)
  if(!req.file){
    return res.status(200).json({ message: 'No file provided.'});
  }
  // if file provided:
  // clearing old file
  if(req.body.oldImagePath){
    fsUtils.clearImage(req.body.oldImagePath);
  }
  // feedback to client for image uploading
  return res.status(201).json({
    message: 'File stored.',
    //(multer parsing form)
    filePath: req.file.path
  })

});

// GraphQL Endpoint
app.use(
  '/graphql',
  graphqlHttp({
    schema: graphqlSchema,
    rootValue: graphqlResolver,
    graphiql: true,
    customFormatErrorFn(err){
      if(!err.originalError){
        return err;
      }
			const data = err.originalError.data;
			const message = err.message || 'An error occurred.';
			const code = err.originalError.code || 500;
      return { message: message, status: code, data: data };
    }
  })
);



// main error handling middleware
app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;

  res.status(status).json({
    message: message,
    data: data
  });
});


mongoose.connect(MONGODB_URL, {useNewUrlParser: true, useFindAndModify: false})
  .then( result => {

    console.log('Connected to mongodb')
    app.listen(port);
    console.log('server started.');

  })
  .catch(err => console.log('Mongodb failed to connect!', err));

