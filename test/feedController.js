const expect = require('chai').expect;
const sinon = require('sinon');
const mongoose = require('mongoose');

const User = require('../models/user');
const feedController = require('../controllers/feed');

// Loading env variables
const dotEnv = require('dotenv').config();
const port = process.env.PORT || '3000';
const MONGODB_PREFIX = process.env.MONGODB_PREFIX;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_ROOT_PASSWORD = process.env.DB_ROOT_PASSWORD;
const DB_HOST = process.env.DB_HOST;
const DB_NAME = process.env.DB_NAME;
const MONGODB_URL_RULE = process.env.MONGODB_URL_RULE;

const MONGODB_URL = `${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/test?${MONGODB_URL_RULE}`


describe('Feed Controller', function(){

  //mongodb connection before all testing block
  before(function(done){
          mongoose.connect(`${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/test?${MONGODB_URL_RULE}`, {useNewUrlParser: true})
      .then( result => {
        const user = new User({
            email: 'test',
            name: 'tester',
            password: 'passwd',
            status: 'I am new!',
            posts: [],
            _id: '5d6c576d341afd0ee87d7c48'
         });

        return user.save();
      })
    .then(() => {
      done();
    });

  });


  describe('feedController - createPost', function(){

    it('should add a created post to the posts of the creator', function(done){

      //code to test
      const req = {
        body: {
          title: 'A wonderful book',
          content: 'testing creating post with relation in user'
        },
        file: {
          path: '/path'
        },
        userId: '5d6c576d341afd0ee87d7c48'
      }

      const res = {
        status: function () {
          return this;
        },
        json: function () {}
      };

      feedController
        .createPost(req, res, () => {})
        .then(saveUser => {
          expect(saveUser).to.have.property('posts');
          expect(saveUser.posts).to.have.length(1);
          done();
        })
        .catch(err => {});
    });
  });


  //clearing testing database
  after(function(done){
    User.deleteMany({})
      .then(() => {
        mongoose.disconnect();
    })
    .then(() => {
      done();
    });
  });

});


