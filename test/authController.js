const expect = require('chai').expect;
const sinon = require('sinon');
const mongoose = require('mongoose');

const User = require('../models/user');
const authController = require('../controllers/auth');

// Loading env variables
const dotEnv = require('dotenv').config();
const port = process.env.PORT || '3000';
const MONGODB_PREFIX = process.env.MONGODB_PREFIX;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_ROOT_PASSWORD = process.env.DB_ROOT_PASSWORD;
const DB_HOST = process.env.DB_HOST;
const DB_NAME = process.env.DB_NAME;
const MONGODB_URL_RULE = process.env.MONGODB_URL_RULE;

const MONGODB_URL = `${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/test?${MONGODB_URL_RULE}`


describe('AuthController', function(){

  //mongodb connection before all testing block
  before(function(done){
          mongoose.connect(`${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/test?${MONGODB_URL_RULE}`, {useNewUrlParser: true})
      .then( result => {
        const user = new User({
            email: 'test',
            name: 'tester',
            password: 'passwd',
            status: 'I am new!',
            posts: [],
            _id: '5d6c576d341afd0ee87d7c48'
         });

        return user.save();
      })
    .then(() => {
      done();
    });

  });


  describe('authController - Login', function(){
    //Stubbing our findOne method inside login method

    it('should throw an error if accessing a database fails', function(done){
      sinon.stub(User, 'findOne', )
      User.findOne.throws();
      //code to test
      const req = {
        body: {
          email: 'test@test.com',
          password: 'passwd'
        }
      }
      authController
        .postLogin(req, {}, () => {})
        .then(result => {
          expect(result).to.be.an('error');
          expect(result).to.have.property('statusCode', 500)
          done();
        })
        .catch(err => {});
      User.findOne.restore();
    });
  });

  describe('authController - status', function(){

    it('should send a response with a valid user status for an existing user', function(done){

      const req = {
        userId: '5d6c576d341afd0ee87d7c48'
      }
      const res = {
        statusCode: 200,
        userStatus: null,
        status: function (code) {
          this.statusCode = code;
          return this;
        },
        json: function (data) {
          this.userStatus = data.status;
        }
      };

      authController.getUserStatus(req, res, ()=>{})
        .then(() => {
          expect(res.statusCode).to.be.equal(200);
          expect(res.userStatus).to.be.equal("I am new!");
          done();
      });
    });
  });

  //clearing testing database
  after(function(done){
    User.deleteMany({})
      .then(() => {
        mongoose.disconnect();
    })
    .then(() => {
      done();
    });
  });

});

