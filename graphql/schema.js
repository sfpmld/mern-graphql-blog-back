const { buildSchema } = require('graphql');

module.exports = buildSchema(`

  type Post {
    _id: ID!
    title: String!
    imageUrl: String!
    content: String!
    creator: User!
    createdAt: String!
    updatedAt: String
  }

  type User {
    _id: ID!
    email: String!
    name: String!
    password: String!
    status: String!
    posts: [Post!]!
    createdAt: String!
    updatedAt: String
  }

  type AuthData {
    token: String!
    userId: String!
  }

  type PostData {
    posts: [Post!]!
    totalPosts: Int!
  }

  type StatusData {
    status: String!
  }

  input UserInput {
    email: String!
    name: String!
    password: String!
  }

  input LoginInput {
    email: String!
    password: String!
  }

  input PostInput {
    title: String!
    imageUrl: String!
    content: String!
  }

  type RootQuery {
    login(dataInput: LoginInput): AuthData!
    posts(page: Int): PostData
    post(id: ID!): Post!
    userStatus(id: ID!): StatusData!
  }

  type RootMutation {
    createUser(dataInput: UserInput): User!
    createPost(dataInput: PostInput): Post!
    updatePost(id: ID!, dataInput: PostInput): Post!
    deletePost(id: ID!): Boolean!
    updateUserStatus(id: ID!, status: String!): Boolean!
  }

  schema {
    query: RootQuery
    mutation: RootMutation
  }
`);
