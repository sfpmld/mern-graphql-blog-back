const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const User = require('../models/user');
const Post = require('../models/post');
const dotenv = require('dotenv').config();

const fsUtils = require('../services/fs-utils');

const JWT_SALT = process.env.JWT_SALT;
const ITEMS_PER_PAGE = +process.env.ITEMS_PER_PAGE;

module.exports = {

  // user signin handler
  createUser: async function({ dataInput }, req) {

    const errors = [];
    // input validation
    if(!validator.isEmail(dataInput.email)){
      errors.push({message: 'E-mail is invalid'});
    }
    if (
      validator.isEmpty(dataInput.password) ||
      !validator.isLength(dataInput.password, { min: 5 })
    ) {
      errors.push({ message: "Password to short!" });
    }
    if(errors.length > 0){
      const error = new Error('Invalid input.');
      error.data = errors;
      error.code = 422;
      throw error;
    }

    const existingUser = await User.findOne({email: dataInput.email});

    // cancel if already exists
    if(existingUser){
      const error = new Error('User already exists.');
      throw error;
    }

    // hashing password
    const hashedpw = await bcrypt.hash(dataInput.password, 12);

    // creating new user
    const user = new User({
      email: dataInput.email,
      name: dataInput.name,
      password: hashedpw
    });

    const createdUser = await user.save();

    return { ...createdUser._doc, _id: createdUser._id.toString()};

  },


  //login user handler
  login: async function({ dataInput }, req){

    const errors = [];
    // input validation
    if(!validator.isEmail(dataInput.email)){
      errors.push({message: 'E-mail is invalid'});
    }
    if (
      validator.isEmpty(dataInput.password) ||
      !validator.isLength(dataInput.password, { min: 5 })
    ) {
      errors.push({ message: "Password to short!" });
    }
    if(errors.length > 0){
      const error = new Error('Invalid input.');
      error.data = errors;
      error.code = 422;
      throw error;
    }

    const user = await User.findOne({email: dataInput.email});

    // if user not found -> error
    if(!user){
      const error = new Error('User not found.');
      error.statusCode = 401;
      throw error;
    }

    //password checking
    const areSame = await bcrypt.compare(dataInput.password, user.password);
    if(!areSame){
      const error = new Error('Password is incorrect');
      error.statusCode = 401;
      throw error;
    }
    //creating token
    const token = jwt.sign(
      {
        userId: user._id.toString(),
        email: user.email
      },
      JWT_SALT,
      { expiresIn: '1h' }
    );

    return { token: token, userId: user._id.toString() };
  },

  // creating post
  createPost: async function({ dataInput }, req){

    // if(!req.isAuth){
    //   const error = new Error('Not Authenticated');
    //   error.code = 401;
    //   throw error;
    // }

    const errors = [];
    // Error test validation block
    if(validator.isEmpty(dataInput.title) || !validator.isLength(dataInput.title, {min: 5})){
      errors.push({ message: 'Title is invalid.' });
    }
    if(validator.isEmpty(dataInput.content) || !validator.isLength(dataInput.content, {min: 5})){
      errors.push({ message: 'Content is invalid.' });
    }
    if(errors.length > 0){
      const error = new Error('Invalid input.');
      error.data = errors;
      error.code = 422;
      throw error;
    }

    //retrieving user info
    const userId = req.userId;
    const user = await User.findById(userId);
    if(!user){
      const error = new Error('Invalid User.');
      error.statusCode = 401;
      throw error;
    }

    //post creation
    const newPost = new Post({
      title: dataInput.title,
      imageUrl: dataInput.imageUrl,
      content: dataInput.content,
      //setting up FK Post to User
      creator: user
    });

    const createdPost = await newPost.save();

    // Setting up FK User to Post
    user.posts.push(createdPost);
    await user.save();

    //Formatting result because graphQL only understand strings and number
    return {
      ...createdPost._doc,
      _id: createdPost._id.toString(),
      createdAt: createdPost.createdAt.toISOString(),
      updatedAt: createdPost.updatedAt.toISOString()
    };

  },

  // view all posts
  posts: async function({ page }, req){

    if(!req.isAuth){
      const error = new Error('Not authenticated.');
      error.code = 401;
      throw error;
    }

    if(!page){
      page = 1;
    }
    const totalPosts = await Post.estimatedDocumentCount();
    const posts = await Post.find()
      .skip((page - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE)
      .sort("-1")
      .populate("creator");

    return {
      posts: posts.map(p => {
        return {
          ...p._doc,
          _id: p._id.toString(),
          createdAt: p.createdAt.toISOString(),
          updatedAt: p.updatedAt.toISOString()
        };
      }),
      totalPosts: totalPosts
    };

  },

  // view single post
  post: async function({ id }, req){

    // check if user is authorized
    if(!req.isAuth){
      const error = new Error('Not Authenticated');
      error.statusCode = 401;
      throw error;
    }

    const post = await Post.findById(id).populate('creator');
    // error if post not found
    if(!post){
      const error = new Error('No post found');
      error.statusCode = 204;
      throw error;
    }

    return {
      ...post._doc,
      _id: post._id.toString(),
      createdAt: post.createdAt.toISOString(),
      updatedAt: post.updatedAt.toISOString()
    }

  },

  // update post handling method
  updatePost: async function({ id, dataInput }, req){

    // checking if user authorized
    if(!req.isAuth){
      const error = new Error('Not Authenticated.');
      error.statusCode = 401;
      throw error;
    }

    const post = await Post.findById(id).populate('creator');
    // checking if post exist
    if(!post){
      const error = new Error('Post not found.');
      error.statusCode = 204;
      throw error;
    }

    // checking if user is authorized to update this post
    if(post.creator._id.toString() !== req.userId.toString()){
      const error = new Error('User not authorized to update this post.');
      error.statusCode = 401;
      throw error;
    }

    // validation block
    const errors = [];
    // Error test validation block
    if(validator.isEmpty(dataInput.title) || !validator.isLength(dataInput.title, {min: 5})){
      errors.push({ message: 'Title is invalid.' });
    }
    if(validator.isEmpty(dataInput.content) || !validator.isLength(dataInput.content, {min: 5})){
      errors.push({ message: 'Content is invalid.' });
    }
    if(errors.length > 0){
      const error = new Error('Invalid input.');
      error.data = errors;
      error.code = 422;
      throw error;
    }

    // erase old image if imageUrl was filled and update imageUrl
    if(dataInput.imageUrl !== 'undefined'){
      post.image = dataInput.imageUrl;
    }

    // updating post values
    post.title = dataInput.title;
    post.content = dataInput.content;

    const updatedPost = await post.save();

    return {
      ...updatedPost._doc,
      _id: updatedPost._id.toString(),
      createdAt: updatedPost.createdAt.toISOString(),
      updatedAt: updatedPost.updatedAt.toISOString()
    }

  },

  deletePost: async function({ id }, req){

    // check if user is authenticated
    if(!req.isAuth){
      const error = new Error('User not authorized.');
      error.statusCode = 401;
      throw error;
    }

    // finding post
    const post = await Post.findById(id);

    if(!post){
      const error = new Error('No posts found.');
      error.statusCode = 204;
      throw error;
    }

    // check if user is authorized to delete this post
    if(post.creator.toString() !== req.userId.toString()){
      const error = new Error('User not authorized to delete this post');
      error.statusCode = 401;
      throw error;
    }

    // deleting post reference in User document
    const user = await User.findById(req.userId)
    // user.posts.pull(id);
    user.posts = user.posts.filter(post => {
      return post._id != id;
    })

    // deleting post
    const deletedPost = await Post.findOneAndDelete({_id: id});

    // remove image file
    fsUtils.clearImage(deletedPost.imageUrl);

    return deletedPost ? true : false;

  },


  userStatus: async function({ id }, req){

    // check if user is Authenticated
    if(!req.isAuth){
      const error = new Error('Not Authenticated');
      error.statusCode = 401;
      throw error;
    }

    // find user
    const user = await User.findById(id);

    return {
      status: user.status
    }

  },

  updateUserStatus: async function({ id, status }, req){

    // check if user is Authenticated
    if(!req.isAuth){
      const error = new Error('Not Authenticated');
      error.statusCode = 401;
      throw error;
    }

    // find user
    const user = await User.findById(id)

    // updating status
    user.status = status;
    const result = await user.save();

    return result ? true : false;

  },

};
