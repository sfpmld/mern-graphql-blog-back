const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();

const JWT_SALT = process.env.JWT_SALT;

module.exports = (req, res, next) => {

  const AuthHeader = req.get('Authorization');

  if(!AuthHeader){
    req.isAuth = false;
    return next();
  }
  const token = AuthHeader.split(' ')[1]
  let decodedToken;

  try {
    decodedToken = jwt.verify(token, JWT_SALT)
  } catch (err) {
    req.isAuth = false;
    return next();
  }

  if(!decodedToken){
    req.isAuth = false;
    return next();
  }

  req.userId = decodedToken.userId;
  req.isAuth = true;
  next();
}

